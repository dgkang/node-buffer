/**
 * add int64 support for node Buffer, dependent on node-int64。
 */

var Int64 = require("./Int64.js");

Buffer.prototype.writeInt64BE = function(value,offset){
	offset = offset || 0;
	int64 = new Int64(this,offset);
	int64.setValue(value);
}

Buffer.prototype.readInt64BE = function(offset){
	offset = offset || 0;
	int64 = new Int64(this,offset);
	return int64.toNumber();
}

Buffer.prototype.writeInt64LE = function(value,offset){
	offset = offset || 0;
	int64 = new Int64(this,offset,false);
	int64.setValue(value);
}

Buffer.prototype.readInt64LE = function(offset){
	offset = offset || 0;
	int64 = new Int64(this,offset,false);
	return int64.toNumber();
}
