add int64 support for node Buffer class, dependent on node-int64。

node-int64:
Support for representing 64-bit integers in JavaScript,
forked from https://github.com/broofa/node-int64.
new add: support little endian
fix: toNumber a bug
