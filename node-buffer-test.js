require("./index.js");


var b = new Buffer(14);
b.writeInt64LE(17179869184);
b.writeInt32LE(45343,8);
b.writeInt16LE(123,12);

console.log(b.readInt64LE());
console.log(b.readInt32LE(8));
console.log(b.readInt16LE(12));
console.log(b);


b.writeInt64BE(-17179869184);
b.writeInt32BE(-45343,8);
b.writeInt16BE(-123,12);

console.log(b.readInt64BE());
console.log(b.readInt32BE(8));
console.log(b.readInt16BE(12));
console.log(b);
